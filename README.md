# FakeBusinessman

FakeBusinessman is a simple trade game, which consists of buying and selling all kinds of goods in cities around the world. Players can buy and sell goods, borrow money.

## Goals

- [X] using TypeScript
- [ ] minimum 50% of code coverage
- [ ] separation of domain from application
- [ ] works on any iPhone
- [ ] each game is different from previous (some random elements)
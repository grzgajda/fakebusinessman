import { IPurchaseCompleted } from './purchaseProduct';
import { IDisposalCompleted } from './sellProduct';

type IAccountBalanceAction = IPurchaseCompleted | IDisposalCompleted;

export default IAccountBalanceAction;

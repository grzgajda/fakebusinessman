import { Action } from 'redux';
import { IProductPrice } from '../../model/products';
import { AccountBalanceActions } from '../../reducers/accountBalance';

const requestProductPurchase = (product: IProduct, quantity: number): IRequestProductPurchase => ({
  product,
  quantity,
  type: AccountBalanceActions.RequestProductPurchase
});
const purchaseCompleted = (product: IProduct, quantity: number): IPurchaseCompleted => ({
  product,
  quantity,
  type: AccountBalanceActions.PurchaseCompleted
});

export { requestProductPurchase, purchaseCompleted };

export interface IRequestProductPurchase extends Action<AccountBalanceActions.RequestProductPurchase> {
  product: IProduct;
  quantity: number;
}
export interface IPurchaseCompleted extends Action<AccountBalanceActions.PurchaseCompleted> {
  product: IProduct;
  quantity: number;
}
interface IProduct extends IProductPrice {
  name: string;
}

import { Action } from 'redux';
import { IProductPrice } from '../../model/products';
import { AccountBalanceActions } from '../../reducers/accountBalance';

const requestProductDisposal = (product: IProduct, quantity: number): IRequestProductDisposal => ({
  product,
  quantity,
  type: AccountBalanceActions.RequestProductDisposal
});

const disposalCompleted = (product: IProduct, quantity: number): IDisposalCompleted => ({
  product,
  quantity,
  type: AccountBalanceActions.DisposalCompleted
});

export { disposalCompleted, requestProductDisposal };

export interface IRequestProductDisposal extends Action<AccountBalanceActions.RequestProductDisposal> {
  product: IProduct;
  quantity: number;
}
export interface IDisposalCompleted extends Action<AccountBalanceActions.DisposalCompleted> {
  product: IProduct;
  quantity: number;
}

interface IProduct extends IProductPrice {
  name: string;
}

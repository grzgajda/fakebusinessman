import { IChangeProductsInCity } from './changeProducts';
import { IMovePlayerToAnotherCity } from './travelToCity';

type ICitiesStockAction = IChangeProductsInCity | IMovePlayerToAnotherCity;

export default ICitiesStockAction;

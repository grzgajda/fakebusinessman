import { Action } from 'redux';
import { IProductPrice } from '../../model/products';
import { CitiesStockAction } from '../../reducers/citiesStock';

const changeProductsInCity = (products: IProductPrice[]): IChangeProductsInCity => ({
  products,
  type: CitiesStockAction.ChangeProductsInCity
});

export { changeProductsInCity };

export interface IChangeProductsInCity extends Action<CitiesStockAction.ChangeProductsInCity> {
  products: IProductPrice[];
}

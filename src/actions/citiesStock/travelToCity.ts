import { Action } from 'redux';
import { CitiesStockAction } from '../../reducers/citiesStock';

const requestTravelToAnotherCity = (cityName: string): IRequestTravelToAnotherCity => ({
  cityName,
  type: CitiesStockAction.RequestTravelToAnotherCity
});

const movePlayerToAnotherCity = (cityName: string): IMovePlayerToAnotherCity => ({
  cityName,
  type: CitiesStockAction.MovePlayerToAnotherCity
});

export { requestTravelToAnotherCity, movePlayerToAnotherCity };

export interface IRequestTravelToAnotherCity extends Action<CitiesStockAction.RequestTravelToAnotherCity> {
  cityName: string;
}
export interface IMovePlayerToAnotherCity extends Action<CitiesStockAction.MovePlayerToAnotherCity> {
  cityName: string;
}

import { IDisableNotifyInterface } from './disableNotify';
import { INotifyPlayerInterface } from './notifyPlayer';
import { ISwitchMultiplierInterface } from './switchMultiplier';
import { ISwitchSelectedViewInterface } from './switchSelectedView';

type UserInterfaceAciton =
  | ISwitchSelectedViewInterface
  | ISwitchMultiplierInterface
  | INotifyPlayerInterface
  | IDisableNotifyInterface;

export default UserInterfaceAciton;

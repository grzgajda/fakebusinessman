import { Action } from 'redux';
import { UserInterfaceActions } from '../../reducers/userInterface';

export default (message: string): IDisableNotifyInterface => ({
  message,
  type: UserInterfaceActions.DisableNotify
});

export interface IDisableNotifyInterface extends Action<UserInterfaceActions.DisableNotify> {
  message: string;
}

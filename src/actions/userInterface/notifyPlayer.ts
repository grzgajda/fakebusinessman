import { Action } from 'redux';
import { UserInterfaceActions } from '../../reducers/userInterface';

export default (message: string, hideAfter: number = 2000): INotifyPlayerInterface => ({
  hideAfter,
  message,
  type: UserInterfaceActions.NotifyPlayer
});

export interface INotifyPlayerInterface extends Action<UserInterfaceActions.NotifyPlayer> {
  message: string;
  hideAfter: number;
}

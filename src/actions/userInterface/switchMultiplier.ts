import { Action } from 'redux';
import { UserInterfaceActions } from '../../reducers/userInterface';

export default (multiplier: number): ISwitchMultiplierInterface => ({
  multiplier,
  type: UserInterfaceActions.ChangeMultiplier
});

export interface ISwitchMultiplierInterface extends Action<UserInterfaceActions.ChangeMultiplier> {
  multiplier: number;
}

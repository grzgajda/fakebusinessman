import { Action } from 'redux';
import { UserInterfaceActions } from '../../reducers/userInterface';

export default (index: number): ISwitchSelectedViewInterface => ({
  index,
  type: UserInterfaceActions.SwitchView
});

export interface ISwitchSelectedViewInterface extends Action<UserInterfaceActions.SwitchView> {
  index: number;
}

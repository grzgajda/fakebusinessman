import background1x from '../../assets/images/background/background.png';
import background15x from '../../assets/images/background/background@1.5x.png';
import background2x from '../../assets/images/background/background@2x.png';
import styled from '../../theme/styled';

const moveBackground = (props: IBackgroundPosition): number => {
  return props.x;
};

const Background = styled.section<IBackgroundPosition>`
  width: 100vw;
  height: 100vh;

  overflow: hidden;

  background-image: url(${background1x});
  background-position-x: ${moveBackground}%;
  background-size: cover;

  @media (min-width: 375px) {
    background-image: url(${background15x});
  }

  @media (min-width: 414px) {
    background-image: url(${background2x});
  }
`;

export default Background;
export interface IBackgroundPosition {
  x: number;
}

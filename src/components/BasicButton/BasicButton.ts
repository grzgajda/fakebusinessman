import button1x from '../../assets/images/button/button.png';
import button15x from '../../assets/images/button/button@1.2x.png';
import button1xSelected from '../../assets/images/button/button_selected.png';
import button15xSelected from '../../assets/images/button/button_selected@1.2x.png';
import bgImg from '../../theme/bgImg';
import flex from '../../theme/flex';
import styled, { ITheme } from '../../theme/styled';
import isSelected, { IIsSelected } from './utils/isSelected';

const BasicButton = styled.button<IIsSelected>`
  ${flex}
  ${props => bgImg(80, 53, isSelected(button1xSelected, button1x)(props))}

  border: none;
  outline: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  background-color: rgba(0, 0, 0, 0);

  color: ${(props: ITheme & IIsSelected) =>
    props.isSelected ? props.theme.colors.lightPrimary : props.theme.colors.primary};
  font-size: 18px;
  text-shadow: 2px 2px 0 black;

  & + & {
    margin-left: 10px;
  }

  @media (min-width: 375px) {
    ${props => bgImg(96, 64, isSelected(button15xSelected, button15x)(props))}
    font-size: 24px;
  }
`;

export default BasicButton;

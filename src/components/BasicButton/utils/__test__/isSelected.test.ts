import isSelected from '../isSelected';

it('should return first argument when condition is true', () => {
  const result = isSelected('is true', 'is false')({ isSelected: true });
  expect(result).toEqual('is true');
});

it('should return second argument when condition is false', () => {
  const result = isSelected('is true', 'is false')({ isSelected: false });
  expect(result).toEqual('is false');
});

it('should return false when props is undefined', () => {
  const result = isSelected('is true', 'is false')({});
  expect(result).toEqual('is false');
});

const isSelected = (ifTrue: string, ifFalse: string) => (props: IIsSelected): string =>
  props.isSelected === true ? ifTrue : ifFalse;

export default isSelected;
export interface IIsSelected {
  isSelected?: boolean;
}

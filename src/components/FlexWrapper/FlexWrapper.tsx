import flex from '../../theme/flex';
import styled from '../../theme/styled';
import marginOrResult, { IWithMarginProps } from './utils/marginOrDefault';

const FlexWrapper = styled.div<IWithMarginProps>`
  ${flex}

  margin-top: ${marginOrResult('mt', 0)}px;
  margin-right: ${marginOrResult('mr', 0)}px;
  margin-bottom: ${marginOrResult('mb', 0)}px;
  margin-left: ${marginOrResult('ml', 0)}px;

  cursor: ${props => (props.onClick !== undefined ? 'pointer' : 'normal')};
`;

const Row = styled(FlexWrapper)`
  flex-direction: row;
`;

const WrappedRow = styled(Row)`
  flex-wrap: wrap;
`;

const Column = styled(FlexWrapper)`
  flex-direction: column;
`;

export default FlexWrapper;
export { Column, WrappedRow, Row, IWithMarginProps };

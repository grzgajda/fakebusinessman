import FlexWrapper, { Column, IWithMarginProps, Row, WrappedRow } from './FlexWrapper';

export default FlexWrapper;
export { Column, IWithMarginProps, Row, WrappedRow };

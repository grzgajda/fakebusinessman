import marginOrResult from '../marginOrDefault';

it('should return value for margin top', () => {
  const result = marginOrResult('mt', 0)({ mt: 20 });
  expect(result).toEqual(20);
});

it('should return value for margin bottom', () => {
  const result = marginOrResult('mb', 0)({ mb: 5 });
  expect(result).toEqual(5);
});

it('should return value for margin right', () => {
  const result = marginOrResult('mr', 0)({ mr: 100 });
  expect(result).toEqual(100);
});

it('should return value for margin left', () => {
  const result = marginOrResult('ml', 0)({ ml: 150 });
  expect(result).toEqual(150);
});

it('should return default value when margin is undefined', () => {
  const result = marginOrResult('mt', 30)({ mr: 40 });
  expect(result).toEqual(30);
});

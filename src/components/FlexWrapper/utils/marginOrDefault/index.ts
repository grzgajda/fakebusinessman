const marginOrResult = (margin: 'mt' | 'mr' | 'mb' | 'ml', defaultValue: number) => (props: IWithMarginProps) =>
  props[margin] || defaultValue;

export default marginOrResult;
export interface IWithMarginProps {
  mt?: number;
  mr?: number;
  mb?: number;
  ml?: number;
}

import bloodImage from '../../assets/images/map/blood.png';
import fabricImage from '../../assets/images/map/fabric.png';
import ironImage from '../../assets/images/map/iron.png';
import scaleImage from '../../assets/images/map/scale.png';
import waterImage from '../../assets/images/map/water.png';
import woodImage from '../../assets/images/map/wood.png';
import bgImg from '../../theme/bgImg';
import styled from '../../theme/styled';

const images = {
  blood: bloodImage,
  fabric: fabricImage,
  iron: ironImage,
  scale: scaleImage,
  water: waterImage,
  wood: woodImage
};

const MapBackground = styled.div<IBackgroundProps>`
  ${(props: IBackgroundProps) => bgImg(298, 298, images[props.resourceImage])}
`;

export default MapBackground;
export interface IBackgroundProps {
  resourceImage: 'blood' | 'fabric' | 'iron' | 'scale' | 'water' | 'wood';
}

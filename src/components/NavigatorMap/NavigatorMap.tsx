import * as React from 'react';
import { Column, Row } from '../../components/FlexWrapper';
import MapBackground, { IBackgroundProps } from './MapBackground';
import { PrimaryPath, ToRight, ToTop } from './Path';

const findCity = (cities: ICityName[], position: number): ICityName => {
  const [city] = cities.filter(c => c.position === position).slice(-1);
  return city;
};

const NavigatorMap = ({ cities, selectedCity, resourceImage, travelToCity }: INavMapProps & INavMapDispatch) => (
  <Column>
    <MapBackground resourceImage={resourceImage} />
    <Column mt={10} ml={35}>
      <Row>
        <ToRight onClick={travelToCity(findCity(cities, 0).name)} isSelected={selectedCity === findCity(cities, 0)} />
        <ToRight onClick={travelToCity(findCity(cities, 1).name)} isSelected={selectedCity === findCity(cities, 1)} />
        <PrimaryPath
          onClick={travelToCity(findCity(cities, 2).name)}
          isSelected={selectedCity === findCity(cities, 2)}
        />
      </Row>
      <Row mt={9}>
        <ToRight onClick={travelToCity(findCity(cities, 5).name)} isSelected={selectedCity === findCity(cities, 5)} />
        <ToRight onClick={travelToCity(findCity(cities, 4).name)} isSelected={selectedCity === findCity(cities, 4)} />
        <ToTop onClick={travelToCity(findCity(cities, 3).name)} isSelected={selectedCity === findCity(cities, 3)} />
      </Row>
    </Column>
  </Column>
);

export default NavigatorMap;
export interface INavMapProps extends IBackgroundProps {
  selectedCity: ICityName;
  cities: ICityName[];
}
export interface INavMapDispatch {
  travelToCity: (cityName: string) => any;
}
interface ICityName {
  name: string;
  position: number;
}

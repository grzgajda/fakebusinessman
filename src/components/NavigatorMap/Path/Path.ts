import flex from '../../../theme/flex';
import styled from '../../../theme/styled';
import { IIsSelected } from '../utils/pathIsSelected';

const Path = styled.button<IIsSelected>`
  ${flex}
  background-color: rgba(0,0,0,0);
  border: none;
  outline: none;

  &:hover {
    cursor: pointer;
  }
`;

export default Path;

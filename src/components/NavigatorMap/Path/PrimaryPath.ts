import primary1x from '../../../assets/images/navigation/from_map.png';
import primary1xSelected from '../../../assets/images/navigation/from_map_selected.png';
import styled from '../../../theme/styled';
import pathIsSelected from '../utils/pathIsSelected';
import Path from './Path';

const PrimaryPath = styled(Path)`
  ${pathIsSelected(66, 134, primary1xSelected, primary1x)}
  margin-top: calc(-134px / 2);
`;

export default PrimaryPath;

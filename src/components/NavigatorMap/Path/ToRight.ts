import toRight1x from '../../../assets/images/navigation/to_right.png';
import toRight1xSelected from '../../../assets/images/navigation/to_right_selected.png';
import styled from '../../../theme/styled';
import pathIsSelected from '../utils/pathIsSelected';
import Path from './Path';

const ToRight = styled(Path)`
  ${pathIsSelected(75, 66, toRight1xSelected, toRight1x)}
`;

export default ToRight;

import toTop1x from '../../../assets/images/navigation/to_top.png';
import toTop1xSelected from '../../../assets/images/navigation/to_top_selected.png';
import styled from '../../../theme/styled';
import pathIsSelected from '../utils/pathIsSelected';
import Path from './Path';

const ToTop = styled(Path)`
  ${pathIsSelected(66, 75, toTop1xSelected, toTop1x)}
  margin-top: -9px;
`;

export default ToTop;

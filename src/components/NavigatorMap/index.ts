import NavigatorMap, { INavMapDispatch, INavMapProps } from './NavigatorMap';

export default NavigatorMap;
export { INavMapDispatch, INavMapProps };

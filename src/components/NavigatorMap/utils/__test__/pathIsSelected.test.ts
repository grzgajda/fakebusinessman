import pathIsSelected from '../pathIsSelected';

it('should return first background when condition is met', () => {
  const result = pathIsSelected(20, 20, 'lorem-ipsum.png', 'dolor-sit-amet.png')({ isSelected: true });
  expect(result).toContain('lorem-ipsum.png');
});

it('should return second background when condition is not met', () => {
  const result = pathIsSelected(20, 20, 'lorem-ipsum.png', 'dolor-sit-amet.png')({ isSelected: false });
  expect(result).toContain('dolor-sit-amet.png');
});

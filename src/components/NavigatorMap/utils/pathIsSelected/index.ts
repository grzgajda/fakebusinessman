import bgImg from '../../../../theme/bgImg';

const isSelected = (width: number | 'auto', height: number | 'auto', ifTrue: string, ifFalse: string) => (
  props: IIsSelected
) => bgImg(width, height, props.isSelected ? ifTrue : ifFalse);

export default isSelected;
export interface IIsSelected {
  isSelected?: boolean;
}

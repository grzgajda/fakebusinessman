import styled from '../../theme/styled';

const ActionButton = styled.button`
  border: none;
  outline: none;
  background: none;
  right: 0;
  top: 0;
  position: absolute;
  width: 45px;
  height: 80px;
`;

export default ActionButton;

import notification from '../../assets/images/notification/notification.png';
import bgImg from '../../theme/bgImg';
import styled from '../../theme/styled';

const Background = styled.div`
  position: relative;
  ${bgImg(300, 78, notification)}
`;

export default Background;

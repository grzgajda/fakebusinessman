import * as React from 'react';
import ActionButton from './ActionButton';
import Background from './Background';
import Overlay from './Overlay';
import Text from './Text';

const Notification = ({ level, message, hide, hideAfter }: INotificationProps & INotificationDispatch) => (
  <React.Fragment>
    {'' !== message && (
      <Overlay>
        <Background>
          <Text>{message}</Text>
          <ActionButton onClick={hide(message)} />
        </Background>
      </Overlay>
    )}
  </React.Fragment>
);

export default Notification;
export interface INotificationProps {
  level: 'info' | 'warning' | 'error';
  message: string;
  hideAfter?: number;
}
export interface INotificationDispatch {
  hide: (message: string) => (event: any) => any;
}

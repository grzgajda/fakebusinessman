import flex from '../../theme/flex';
import styled from '../../theme/styled';

const Overlay = styled.section`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  background: rgba(0, 0, 0, 0.7);
  touch-action: none;
  user-select: none;
  ${flex}
`;

export default Overlay;

import flex from '../../theme/flex';
import styled from '../../theme/styled';

const Text = styled.span`
  width: calc(100% - 65px);
  height: 100%;
  padding-left: 20px;
  padding-top: 13px;
  padding-bottom: 13px;

  ${flex}
`;

export default Text;

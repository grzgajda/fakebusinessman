import Notification, { INotificationDispatch, INotificationProps } from './Notification';

export default Notification;
export { INotificationDispatch, INotificationProps };

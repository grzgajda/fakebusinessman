export default interface IActionProps {
  children: string | number;
  label: string;
}

import * as React from 'react';
import { IRequestProductPurchase } from '../../../actions/accountBalance/purchaseProduct';
import { Column } from '../../FlexWrapper';
import IActionProps from './Action';
import Label from './Label';
import Text from './Text';

const Action = ({ children, label, buyAction }: IActionProps & IBuyActionDispatch) => (
  <Column onClick={buyAction}>
    <Label>{label}</Label>
    <Text>
      <Label>x</Label>
      {children}
    </Text>
  </Column>
);

export default Action;
export interface IBuyActionDispatch {
  buyAction?: () => IRequestProductPurchase;
}

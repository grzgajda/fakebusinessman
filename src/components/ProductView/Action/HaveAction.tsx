import * as React from 'react';
import { Column } from '../../FlexWrapper';
import IActionProps from './Action';
import Label from './Label';
import Text from './Text';

const Action = ({ children, label }: IActionProps) => (
  <Column>
    <Label>{label}</Label>
    <Text>{children}</Text>
  </Column>
);

export default Action;

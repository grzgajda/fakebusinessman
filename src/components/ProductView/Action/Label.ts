import styled from '../../../theme/styled';

const Label = styled.small`
  font-size: 11px;
`;

export default Label;

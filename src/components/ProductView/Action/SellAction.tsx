import * as React from 'react';
import { IRequestProductDisposal } from '../../../actions/accountBalance/sellProduct';
import { Column } from '../../FlexWrapper';
import IActionProps from './Action';
import Label from './Label';
import Text from './Text';

const Action = ({ children, label, sellAction }: IActionProps & ISellActionDispatch) => (
  <Column onClick={sellAction}>
    <Label>{label}</Label>
    <Text>
      <Label>x</Label>
      {children}
    </Text>
  </Column>
);

export default Action;
export interface ISellActionDispatch {
  sellAction?: () => IRequestProductDisposal;
}

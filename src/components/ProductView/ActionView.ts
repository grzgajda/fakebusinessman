import styled from '../../theme/styled';
import { Row } from '../FlexWrapper';

const ActionView = styled(Row)`
  width: 100%;
  height: 100%;
  max-width: 128px;
  max-height: 34px;
  margin: 0 6px 6px 6px;

  & > div {
    width: calc(128px / 3);
    &:first-child {
      padding-right: 2px;
    }
    &:last-child {
      padding-left: 2px;
    }
  }

  @media (min-width: 375px) {
    max-width: 151px;
    max-height: 40px;
    margin: 0px 6px 7px 6px;

    & > div {
      width: calc(151px / 3);
      &:first-child {
        padding-right: 3px;
      }
      &:last-child {
        padding-left: 3px;
      }
    }
  }
`;

export default ActionView;

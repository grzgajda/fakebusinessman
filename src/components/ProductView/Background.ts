import productBg from '../../assets/images/product/product.png';
import productBg15x from '../../assets/images/product/product@1.2x.png';
import bgImg from '../../theme/bgImg';
import styled from '../../theme/styled';
import { Column } from '../FlexWrapper';

const Background = styled(Column)`
  ${bgImg(140, 120, productBg)}
  justify-content: flex-start;
  margin-top: 10px;

  &:nth-child(2n-1) {
    margin-right: 10px;
  }

  @media (min-width: 375px) {
    ${bgImg(168, 144, productBg15x)}
  }
`;

export default Background;

import styled, { ITheme } from '../../../theme/styled';

const Label = styled.small`
  font-size: 22px;
  color: ${(props: ITheme) => props.theme.colors.primary};
  text-shadow: 1px 1px 0px black;
`;

export default Label;

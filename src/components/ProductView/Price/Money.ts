import styled, { ITheme } from '../../../theme/styled';

const Money = styled.strong`
  color: ${(props: ITheme) => props.theme.colors.primary};
  font-size: 30px;
  line-height: 30px;
  text-shadow: 2px 2px 0px black;
`;

export default Money;

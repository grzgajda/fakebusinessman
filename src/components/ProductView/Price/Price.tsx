import * as React from 'react';
import Label from './Label';
import Money from './Money';
import Wrapper from './Wrapper';

const Price = ({ label, children }: IPriceProps) => (
  <Wrapper>
    <Label>{label}</Label>
    <Money>{children}</Money>
  </Wrapper>
);

export default Price;
export interface IPriceProps {
  label: string;
  children: number | string;
}

import styled from '../../../theme/styled';
import { Column } from '../../FlexWrapper';

const Wrapper = styled(Column)`
  width: 100%;
`;

export default Wrapper;

import styled from '../../theme/styled';

const ProductThumbnail = styled.img`
  max-height: 55px;
`;

export default ProductThumbnail;

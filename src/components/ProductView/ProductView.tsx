import * as React from 'react';
import SwipeableViews from 'react-swipeable-views';
import formatNumber from '../../utils/formatNumber';
import { Row } from '../FlexWrapper';
import { BuyAction, HaveAction, IBuyActionDispatch, ISellActionDispatch, SellAction } from './Action';
import ActionView from './ActionView';
import Background from './Background';
import Price from './Price';
import ProductThumbnail from './ProductThumbnail';
import ThumbnailView, { ThumbnailWrapper } from './ThumbnailView';

const ProductView = ({
  buyAction,
  buyRate,
  haveNumber,
  sellAction,
  sellRate,
  product
}: IProductViewProps & IProductViewDispatch) => (
  <Background>
    <ThumbnailView>
      <SwipeableViews>
        <ThumbnailWrapper>
          <ProductThumbnail src={product.icon} />
        </ThumbnailWrapper>
        <Row>
          <Price label={'BUY'}>{formatNumber(product.buyPrice * Number(buyRate === 0 ? 1 : buyRate))}</Price>
          <Price label={'SELL'}>{formatNumber(product.sellPrice * Number(sellRate === 0 ? 1 : sellRate))}</Price>
        </Row>
      </SwipeableViews>
    </ThumbnailView>
    <ActionView>
      <BuyAction label={'buy'} buyAction={buyAction}>
        {buyRate}
      </BuyAction>
      <HaveAction label={'have'}>{formatNumber(haveNumber)}</HaveAction>
      <SellAction label={'sell'} sellAction={sellAction}>
        {sellRate}
      </SellAction>
    </ActionView>
  </Background>
);

export default ProductView;
export interface IProductViewProps {
  haveNumber: number;
  buyRate: number | string;
  sellRate: number | string;
  product: IProduct;
}
export interface IProductViewDispatch extends IBuyActionDispatch, ISellActionDispatch {}
interface IProduct {
  icon: string;
  buyPrice: number;
  sellPrice: number;
}

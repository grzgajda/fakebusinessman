import flex from '../../theme/flex';
import styled from '../../theme/styled';

const ThumbnailView = styled.div`
  ${flex}
  width: 100%;
  height: 100%;
  max-width: 128px;
  max-height: 74px;
  margin: 6px 6px 0 6px;

  @media (min-width: 375px) {
    max-width: 151px;
    max-height: 90px;
    margin: 7px 6px 0 6px;
  }
`;

const ThumbnailWrapper = styled.div`
  ${flex}
`;

export default ThumbnailView;
export { ThumbnailWrapper };

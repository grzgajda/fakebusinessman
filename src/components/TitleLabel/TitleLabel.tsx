import image from '../../assets/images/title_label/title_label.png';
import image15x from '../../assets/images/title_label/title_label@1.5x.png';
import bgImg from '../../theme/bgImg';
import flex from '../../theme/flex';
import styled, { ITheme } from '../../theme/styled';

const TitleLabel = styled.header`
  ${bgImg(183, 40, image)}
  ${flex}

  color: ${(props: ITheme) => props.theme.colors.lightPrimary};
  text-shadow: 2px 2px 0px black;
  word-spacing: 4px;

  @media (min-width: 375px) {
    ${bgImg(275, 60, image15x)}
    font-size: 1.5em;
    word-spacing: 7px;
  }
`;

export default TitleLabel;

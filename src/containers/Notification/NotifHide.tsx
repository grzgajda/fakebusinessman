import * as React from 'react';
import Notification, { INotificationDispatch, INotificationProps } from '../../components/Notification';

class NotifHide extends React.Component<INotificationProps & INotificationDispatch> {
  constructor(props: INotificationProps & INotificationDispatch) {
    super(props);
  }

  public componentWillReceiveProps(nextProps: INotificationProps) {
    if (nextProps.message !== '') {
      setTimeout(() => {
        this.props.hide(this.props.message)({});
      }, this.props.hideAfter || 2000);
    }
  }

  public render() {
    return <Notification {...this.props} />;
  }
}

export default NotifHide;

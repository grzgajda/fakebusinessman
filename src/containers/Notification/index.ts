import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import disableNotify from '../../actions/userInterface/disableNotify';
import { INotificationDispatch, INotificationProps } from '../../components/Notification';
import { IStore } from '../../store';
import NotifHide from './NotifHide';

const lastNotifyOrEmpty = (state: IStore): string => {
  const notif = state.userInterface.notification.filter(s => s.wasSelected === false).slice(-1);
  if (notif.length === 0) {
    return '';
  }

  return notif[0].message;
};

const mapStateToProps = (state: IStore): INotificationProps => ({
  level: 'info',
  message: lastNotifyOrEmpty(state)
});

const bindDispatchToProps = (dispatch: Dispatch): INotificationDispatch => ({
  hide: (message: string) => (event: any) => dispatch(disableNotify(message))
});

export default connect(
  mapStateToProps,
  bindDispatchToProps
)(NotifHide);

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { MemoryRouter, Route } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import IndexPage from './screens/IndexPage';
import store, { Provider } from './store';
import { GlobalStyles, theme, ThemeProvider } from './theme/styled';

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <GlobalStyles theme={theme} />
        <MemoryRouter>
          <Route path={'/'} exact={true} component={IndexPage} />
        </MemoryRouter>
      </React.Fragment>
    </ThemeProvider>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();

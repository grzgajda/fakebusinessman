import { ICity } from '../cities';
import { ProductType } from '../products';

export default function({ name, primaryPurchase, position }: IFakeCity): ICity {
  return {
    name: name || 'Lorem ipsum',
    position: position || 0,
    primaryResource: primaryPurchase || 'water'
  };
}

interface IFakeCity {
  name?: string;
  primaryPurchase?: ProductType;
  position?: number;
}

import { IProduct, IProductPrice, ProductType } from '../products';

export default function({ price, buyPrice, icon, name, sellPrice, type }: IFakeProduct): IProduct & IProductPrice {
  return {
    buyPrice: buyPrice || 100,
    icon: icon || 'lorem-ipsum.png',
    name: name || 'Lorem Ipsum',
    price: price || 0,
    sellPrice: sellPrice || 150,
    type: type || 'wood'
  };
}

interface IFakeProduct {
  price?: number;
  buyPrice?: number;
  icon?: string;
  name?: string;
  sellPrice?: number;
  type?: ProductType;
}

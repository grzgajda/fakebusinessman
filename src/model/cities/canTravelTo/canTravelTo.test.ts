import canTravelTo from './canTravelTo';

it('should allow to travel when cities are close to themselves', () => {
  const cityA = { position: 0 };
  const cityB = { position: 1 };
  expect(canTravelTo(cityA, cityB)).toBeTruthy();
});

it('should disallow to travel when cities are far awaay to themselves', () => {
  const cityA = { position: 0 };
  const cityB = { position: 2 };
  expect(canTravelTo(cityA, cityB)).toBeFalsy();
});

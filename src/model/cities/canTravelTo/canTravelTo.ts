import { ICityPosition } from '../index';

const canTravelTo = (from: ICityPosition, to: ICityPosition): boolean => {
  return from.position - 1 === to.position || from.position + 1 === to.position;
};

export default canTravelTo;

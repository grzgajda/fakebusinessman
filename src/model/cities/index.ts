import { ProductType } from '../products';

const cities: ICity[] = [
  { name: 'The Wilderness', primaryResource: 'wood', position: 2 },
  { name: 'The Citadel', primaryResource: 'water', position: 3 },
  { name: 'Theme Park', primaryResource: 'iron', position: 1 },
  { name: 'Wasteland', primaryResource: 'scale', position: 4 },
  { name: 'The Pot', primaryResource: 'fabric', position: 0 },
  { name: 'Garden of Eden', primaryResource: 'blood', position: 5 }
];

export default cities;
export interface ICity extends ICityName, ICityPosition {
  primaryResource: ProductType;
}
export interface ICityName {
  name: string;
}
export interface ITravel {
  toCity: ICityName & ICityPosition;
}
export interface ICityPosition {
  position: number;
}

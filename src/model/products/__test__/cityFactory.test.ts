import randomCity from '../../__test__/randomCity';
import cityFactory from '../factory/cityFactory';
import { IProduct } from '../index';

it('should return list of products even when is empty', () => {
  const results = cityFactory([])(randomCity({ name: 'A', primaryPurchase: 'wood' }));
  expect(results).toHaveLength(0);
});

it('should return list of products even when is incompleted', () => {
  const products: IProduct[] = [{ name: 'A', type: 'wood', price: 100, icon: 'w.png' }];

  const results = cityFactory(products)(randomCity({ name: 'A', primaryPurchase: 'wood' }));
  expect(results).toHaveLength(1);
});

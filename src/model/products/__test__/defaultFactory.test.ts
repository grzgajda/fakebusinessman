import defaultFactory from '../factory/defaultFactory';

it('should return array of all products', () => {
  const results = defaultFactory(0);
  expect(results[0].name).toEqual('Wood');
  expect(results[0].price).toEqual(50);
});

it('should have generated price when with limits', () => {
  const result = defaultFactory(0.3);
  expect(result[0].name).toEqual('Wood');
  expect(result[0].price).toBeGreaterThanOrEqual(35);
  expect(result[0].price).toBeLessThanOrEqual(65);
});

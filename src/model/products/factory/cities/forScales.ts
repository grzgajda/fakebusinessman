import { IObject } from '../utils/objectToMap';

const pricesRanges: IObject[] = [
  { type: 'wood', ratio: 0.8 },
  { type: 'water', ratio: 0.8 },
  { type: 'iron', ratio: 1 },
  { type: 'scale', ratio: 1.2 },
  { type: 'fabric', ratio: 1.2 },
  { type: 'blood', ratio: 1.5 }
];

export default pricesRanges;

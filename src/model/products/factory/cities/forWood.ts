import { IObject } from '../utils/objectToMap';

const pricesRanges: IObject[] = [
  { type: 'wood', ratio: 0 },
  { type: 'water', ratio: 0.1 },
  { type: 'iron', ratio: 0.1 },
  { type: 'scale', ratio: 0.2 },
  { type: 'fabric', ratio: 0.3 },
  { type: 'blood', ratio: 0.3 }
];

export default pricesRanges;

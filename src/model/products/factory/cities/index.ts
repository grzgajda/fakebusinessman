import forBlood from './forBlood';
import forFabric from './forFabric';
import forIron from './forIron';
import forScales from './forScales';
import forWater from './forWater';
import forWood from './forWood';

export { forBlood, forFabric, forIron, forScales, forWater, forWood };

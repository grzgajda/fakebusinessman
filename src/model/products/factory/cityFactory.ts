import { ICity } from '../../cities';
import { IProduct, IProductPrice, ProductType } from '../index';
import { forBlood, forFabric, forIron, forScales, forWater, forWood } from './cities';
import extendProducts from './utils/extendProducts';
import objectToMap from './utils/objectToMap';

const productFactory = (originalProducts: IProduct[]) => (city: ICity): IProductPrice[] => {
  const extended = extendProducts(originalProducts);

  switch (city.primaryResource) {
    case 'blood':
      return extended(objectToMap(forBlood));
    case 'fabric':
      return extended(objectToMap(forFabric));
    case 'iron':
      return extended(objectToMap(forIron));
    case 'scale':
      return extended(objectToMap(forScales));
    case 'water':
      return extended(objectToMap(forWater));
    case 'wood':
      return extended(objectToMap(forWood));
  }
};

export default productFactory;
export interface IPricesRange extends Map<ProductType, IMinMax> {}
interface IMinMax {
  buyRatio: number;
  sellRatio: number;
}

import blood from '../../../assets/images/resources/blood.png';
import fabric from '../../../assets/images/resources/fabric.png';
import iron from '../../../assets/images/resources/iron.png';
import scale from '../../../assets/images/resources/scales.png';
import water from '../../../assets/images/resources/water.png';
import wood from '../../../assets/images/resources/wood.png';
import randomPrice from '../../../utils/randomPrice';
import { IProduct, ProductType } from '../index';

const createDefautProduct = (name: string, type: ProductType, icon: string, price: number): IProduct => ({
  icon,
  name,
  price,
  type
});

export default (randomLimit: number): IProduct[] => [
  createDefautProduct('Wood', 'wood', wood, randomPrice(50, randomLimit)),
  createDefautProduct('Water', 'water', water, randomPrice(75, randomLimit)),
  createDefautProduct('Iron', 'iron', iron, randomPrice(100, randomLimit)),
  createDefautProduct('Scales', 'scale', scale, randomPrice(120, randomLimit)),
  createDefautProduct('Fabric', 'fabric', fabric, randomPrice(150, randomLimit)),
  createDefautProduct('Blood', 'blood', blood, randomPrice(200, randomLimit))
];

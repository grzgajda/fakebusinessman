import randomPrice from '../../../../utils/randomPrice';
import { IProduct, IProductPrice } from '../../index';
import { IPricesRange } from './objectToMap';

const defaultValue = { ratio: 1 };

const extendProducts = (products: IProduct[]) => (definitions: IPricesRange): IProductPrice[] =>
  products.map(product => {
    const price = randomPrice(product.price, (definitions.get(product.type) || defaultValue).ratio);

    return {
      ...product,
      buyPrice: price,
      sellPrice: price * 0.9
    };
  });

export default extendProducts;

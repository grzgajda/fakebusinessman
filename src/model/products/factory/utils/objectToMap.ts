import { ProductType } from '../../index';

const objectToMap = (objects: IObject[]): IPricesRange => {
  const map = new Map<ProductType, IMinMax>();
  objects.forEach(o => map.set(o.type, o));

  return map;
};

export default objectToMap;
export interface IPricesRange extends Map<ProductType, IMinMax> {}
interface IMinMax {
  ratio: number;
}
export interface IObject extends IMinMax {
  type: ProductType;
}

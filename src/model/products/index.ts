import { getRandomArbitrary } from '../../utils/randomPrice';
import { cityFactory, defaultFactory } from './factory';

const randomProducts = defaultFactory(getRandomArbitrary({ min: 0.1, max: 0.5 }));
const forCitiesFactory = cityFactory(randomProducts);

export default randomProducts;
export { forCitiesFactory };

export interface IProduct extends IProductBasic {
  icon: string;
  price: number;
}
export interface IProductPrice extends IProductBasic {
  buyPrice: number;
  sellPrice: number;
}
export interface IProductBasic {
  name: string;
  type: ProductType;
}

export type ProductType = 'wood' | 'water' | 'scale' | 'iron' | 'fabric' | 'blood';

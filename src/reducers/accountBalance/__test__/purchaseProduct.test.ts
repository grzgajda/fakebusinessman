import { purchaseCompleted } from '../../../actions/accountBalance/purchaseProduct';
import randomCityProduct from '../../../model/__test__/randomProduct';
import reducer from '../index';
import initialState, { IAccountBalanceState } from '../initialState';

it('should add new transaction when purchasing product', () => {
  expect(initialState.yourAccount.length).toEqual(1);

  const action = purchaseCompleted(randomCityProduct({ buyPrice: 20 }), 20);
  const newState = reducer(initialState, action);

  expect(newState).not.toBe(initialState);
  expect(newState.yourAccount.length).toEqual(2);
  expect(newState.yourAccount[1].amount).toEqual(-400);
});

it('should not allow to add new transaction when account balance is lower than price', () => {
  const state: IAccountBalanceState = {
    ...initialState,
    yourAccount: [...initialState.yourAccount, { amount: -1000 }]
  };
  expect(initialState.yourAccount.length).toEqual(1);
  expect(state.yourAccount.length).toEqual(2);

  const action = purchaseCompleted(randomCityProduct({ price: 20 }), 20);
  const newState = reducer(state, action);

  expect(newState).toBe(state);
  expect(newState.yourAccount.length).toEqual(2);
});

import initialState from '../initialState';

it('should contain default transaction with bank debt', () => {
  expect(initialState.yourAccount.length).toEqual(1);
  expect(initialState.yourAccount[0].amount).toEqual(1000);
});

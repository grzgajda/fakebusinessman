import { disposalCompleted } from '../../../actions/accountBalance/sellProduct';
import randomCityProduct from '../../../model/__test__/randomProduct';
import countProducts from '../../../utils/countProducts';
import reducer from '../index';
import initialState, { IAccountBalanceState, IPurchaseTransaction } from '../initialState';

it('should add new transaction when selling product', () => {
  const state: IAccountBalanceState = {
    ...initialState,
    yourAccount: [
      ...initialState.yourAccount,
      { amount: -400, product: randomCityProduct({ name: 'Soup' }), quantity: 20 } as IPurchaseTransaction
    ]
  };
  expect(initialState.yourAccount.length).toEqual(1);
  expect(state.yourAccount.length).toEqual(2);

  const action = disposalCompleted(randomCityProduct({ name: 'Soup', sellPrice: 20 }), 10);
  const newState = reducer(state, action);

  expect(newState).not.toBe(initialState);
  expect(newState.yourAccount.length).toEqual(3);
  expect(newState.yourAccount[2].amount).toEqual(200);
  expect(countProducts(newState.yourAccount, 'Soup')).toEqual(10);
});

it('should allow to sell all products which belong to player', () => {
  const state: IAccountBalanceState = {
    ...initialState,
    yourAccount: [
      ...initialState.yourAccount,
      { amount: -400, product: randomCityProduct({}), quantity: 20 } as IPurchaseTransaction
    ]
  };

  expect(initialState.yourAccount.length).toEqual(1);
  expect(state.yourAccount.length).toEqual(2);

  const action = disposalCompleted(randomCityProduct({ sellPrice: 20 }), 20);
  const newState = reducer(state, action);

  expect(newState).not.toBe(state);
  expect(newState.yourAccount.length).toEqual(3);
  expect(newState.yourAccount[2].amount).toEqual(400);
  expect(countProducts(newState.yourAccount, 'Soup')).toEqual(0);
});

it('should not allow to sell products when player has not any products', () => {
  expect(initialState.yourAccount.length).toEqual(1);

  const action = disposalCompleted(randomCityProduct({ sellPrice: 20 }), 20);
  const newState = reducer(initialState, action);

  expect(newState).toBe(initialState);
  expect(newState.yourAccount.length).toEqual(1);
});

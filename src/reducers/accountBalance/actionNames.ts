export enum AccountBalanceActions {
  RequestProductPurchase = 'Player wants to buy product',
  RequestProductDisposal = 'Player wants to sell product',
  PurchaseCompleted = 'Player bought product',
  DisposalCompleted = 'Player sold product',
  PURCHASE_PRODUCT = 'purchases product',
  SELL_PRODUCT = 'sell product'
}

import Action from '../../actions/accountBalance/actionDefinition';
import { AccountBalanceActions } from './actionNames';
import initialState, { IAccountBalanceState } from './initialState';
import purchaseProduct from './mutators/purchaseProduct';
import sellProduct from './mutators/sellProduct';

export default function(state: IAccountBalanceState = initialState, action: Action): IAccountBalanceState {
  switch (action.type) {
    case AccountBalanceActions.PurchaseCompleted:
      return purchaseProduct(state, action);
    case AccountBalanceActions.DisposalCompleted:
      return sellProduct(state, action);
  }
  return state;
}
export { IAccountBalanceState, AccountBalanceActions };

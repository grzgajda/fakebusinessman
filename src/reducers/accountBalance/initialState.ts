const initialState: IAccountBalanceState = {
  bankAccount: [{ amount: -1000 }],
  yourAccount: [{ amount: 1000 }]
};

export default initialState;
export interface IAccountBalanceState {
  yourAccount: ITransaction[];
  bankAccount: ITransaction[];
}
export interface ITransaction {
  amount: number;
}
export interface IPurchaseTransaction extends ITransaction {
  product: IProduct;
  quantity: number;
}
interface IProduct {
  name: string;
  buyPrice: number;
  sellPrice: number;
}

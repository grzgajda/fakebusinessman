import { IPurchaseCompleted } from '../../../actions/accountBalance/purchaseProduct';
import calculateBalance from '../../../utils/calculateBalance';
import { IAccountBalanceState, IPurchaseTransaction } from '../initialState';

export default (state: IAccountBalanceState, action: IPurchaseCompleted): IAccountBalanceState => {
  const currentBalance = calculateBalance(state.yourAccount);
  const product = newProduct(action);

  if (-1 * product.amount > currentBalance) {
    return state;
  }

  return { ...state, yourAccount: [...state.yourAccount, product] };
};

const newProduct = (action: IPurchaseCompleted): IPurchaseTransaction => ({
  amount: -1 * (action.product.buyPrice * action.quantity),
  product: action.product,
  quantity: action.quantity
});

import { IDisposalCompleted } from '../../../actions/accountBalance/sellProduct';
import countProducts from '../../../utils/countProducts';
import { IAccountBalanceState, IPurchaseTransaction } from '../initialState';

export default (state: IAccountBalanceState, action: IDisposalCompleted): IAccountBalanceState => {
  const quantity = countProducts(state.yourAccount, action.product.name);
  const product = newProduct(action);

  if (action.quantity > quantity) {
    return state;
  }

  return { ...state, yourAccount: [...state.yourAccount, product] };
};

const newProduct = (action: IDisposalCompleted): IPurchaseTransaction => ({
  amount: action.product.sellPrice * action.quantity,
  product: action.product,
  quantity: action.quantity
});

import { Action } from 'redux';
import { AchievementsAction } from './actionNames';
import initialState, { IAchievementsState } from './initialState';

export default function(state: IAchievementsState = initialState, action: Action): IAchievementsState {
  return state;
}
export { IAchievementsState, AchievementsAction };

const initialState: IAchievementsState = {
  achiements: [],
  additionals: [],
  warnings: []
};

export default initialState;
export interface IAchievementsState {
  achiements: IAchievement[];
  additionals: IAchievement[];
  warnings: IAchievement[];
}
export interface IAchievement {
  title: string;
}

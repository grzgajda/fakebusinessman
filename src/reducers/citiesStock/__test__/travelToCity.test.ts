import { movePlayerToAnotherCity } from '../../../actions/citiesStock/travelToCity';
import rCity from '../../../model/__test__/randomCity';
import { ICity } from '../../../model/cities';
import reducer from '../index';
import initialState from '../initialState';

it('should add new travel to list', () => {
  expect(initialState.travels.length).toEqual(1);
  const randomCity: ICity = rCity({ name: 'The Boatman Inn', primaryPurchase: 'water' });
  const state = { ...initialState, cities: [...initialState.cities, randomCity] };

  const action = movePlayerToAnotherCity('The Boatman Inn');
  const result = reducer(state, action);

  expect(result.travels.length).toEqual(2);
});

it('should return original state when city with index "26" does not exists', () => {
  const action = movePlayerToAnotherCity('Lorem ipsum dolor sit amet');
  const result = reducer(initialState, action);

  expect(result).toBe(initialState);
});

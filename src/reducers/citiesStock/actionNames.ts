export enum CitiesStockAction {
  RequestTravelToAnotherCity = 'Player wants to travel to another city',
  MovePlayerToAnotherCity = 'Player travelled to another city',
  ChangeProductsInCity = 'Products on market view has been replaced'
}

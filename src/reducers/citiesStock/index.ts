import Action from '../../actions/citiesStock/actionDefinition';
import { CitiesStockAction } from './actionNames';
import initialState, { ICitiesStockState } from './initialState';
import changeProducts from './mutators/changeProducts';
import travelToCity from './mutators/travelToCity';

export default function(state: ICitiesStockState = initialState, action: Action): ICitiesStockState {
  switch (action.type) {
    case CitiesStockAction.MovePlayerToAnotherCity:
      return travelToCity(state, action);
    case CitiesStockAction.ChangeProductsInCity:
      return changeProducts(state, action);
  }

  return state;
}
export { ICitiesStockState, CitiesStockAction };

import cities, { ICity, ITravel } from '../../model/cities';
import { forCitiesFactory, IProductPrice } from '../../model/products';

const initialState: ICitiesStockState = {
  cities,
  resources: [forCitiesFactory(cities[0])],
  travels: [{ toCity: cities[0] }]
};

export default initialState;
export interface ICitiesStockState {
  cities: ICity[];
  travels: ITravel[];
  resources: IProductPrice[][];
}

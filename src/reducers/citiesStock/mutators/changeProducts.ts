import { IChangeProductsInCity } from '../../../actions/citiesStock/changeProducts';
import { ICitiesStockState } from '../initialState';

export default function(state: ICitiesStockState, action: IChangeProductsInCity): ICitiesStockState {
  return { ...state, resources: [...state.resources, action.products] };
}

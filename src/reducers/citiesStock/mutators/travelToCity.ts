import { IMovePlayerToAnotherCity } from '../../../actions/citiesStock/travelToCity';
import { ICitiesStockState } from '../initialState';

export default function(state: ICitiesStockState, action: IMovePlayerToAnotherCity): ICitiesStockState {
  const findCity = state.cities.filter(city => city.name === action.cityName && city.name !== '');
  if (findCity.length === 0) {
    return state;
  }

  return { ...state, travels: [...state.travels, { toCity: findCity[0] }] };
}

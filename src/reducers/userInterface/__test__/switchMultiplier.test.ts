import switchMultiplier from '../../../actions/userInterface/switchMultiplier';
import reducer from '../index';
import initialState from '../initialState';

it('should change selected tab on proper action', () => {
  expect(initialState.selectedMultiplier).toEqual(1);

  const action = switchMultiplier(10);
  const newState = reducer(initialState, action);

  expect(newState).not.toBe(initialState);
  expect(newState.selectedMultiplier).toEqual(10);
});

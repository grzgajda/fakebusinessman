import switchSelectedView from '../../../actions/userInterface/switchSelectedView';
import reducer from '../index';
import initialState from '../initialState';

it('should change selected tab on proper action', () => {
  expect(initialState.selectedView).not.toEqual(3);

  const action = switchSelectedView(3);
  const newState = reducer(initialState, action);

  expect(newState).not.toBe(initialState);
  expect(newState.selectedView).toEqual(3);
});

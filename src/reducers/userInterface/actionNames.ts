export enum UserInterfaceActions {
  ChangeMultiplier = 'Changed multiplier for market',
  NotifyPlayer = 'Show notification for player',
  DisableNotify = 'Hide all notifies for player',
  SwitchView = 'Switched selected view to another'
}

import Action from '../../actions/userInterface/actionDefinition';
import { UserInterfaceActions } from './actionNames';
import initialState, { IUserInterfaceState } from './initialState';
import switchMultiplier from './mutators/switchSelectedMultiplier';
import switchSelectedTab from './mutators/switchSelectedTab';

export default function(state: IUserInterfaceState = initialState, action: Action): IUserInterfaceState {
  switch (action.type) {
    case UserInterfaceActions.SwitchView:
      return switchSelectedTab(state, action);
    case UserInterfaceActions.ChangeMultiplier:
      return switchMultiplier(state, action);
    case UserInterfaceActions.NotifyPlayer:
      return {
        ...state,
        notification: [
          ...state.notification,
          { message: action.message, hideAfter: action.hideAfter, wasSelected: false }
        ]
      };
    case UserInterfaceActions.DisableNotify:
      return {
        ...state,
        notification: state.notification.map(s => {
          s.wasSelected = true;
          return s;
        })
      };
  }

  return state;
}
export { IUserInterfaceState, UserInterfaceActions };

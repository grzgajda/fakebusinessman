const initialState: IUserInterfaceState = {
  notification: [],
  selectedMultiplier: 1,
  selectedView: 1
};

export default initialState;
export interface IUserInterfaceState {
  selectedMultiplier: number;
  selectedView: number;
  notification: INotification[];
}
export interface INotification {
  message: string;
  hideAfter?: number;
  wasSelected: boolean;
}

import { ISwitchMultiplierInterface } from '../../../actions/userInterface/switchMultiplier';
import { IUserInterfaceState } from './../initialState';

export default (state: IUserInterfaceState, action: ISwitchMultiplierInterface): IUserInterfaceState => ({
  ...state,
  selectedMultiplier: action.multiplier
});

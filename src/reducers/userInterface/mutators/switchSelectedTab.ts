import { ISwitchSelectedViewInterface } from '../../../actions/userInterface/switchSelectedView';
import { IUserInterfaceState } from './../initialState';

export default (state: IUserInterfaceState, action: ISwitchSelectedViewInterface): IUserInterfaceState => ({
  ...state,
  selectedView: action.index
});

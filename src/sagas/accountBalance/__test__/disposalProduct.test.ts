import { put } from 'redux-saga/effects';
import { disposalCompleted, requestProductDisposal } from '../../../actions/accountBalance/sellProduct';
import randomCityProduct from '../../../model/__test__/randomProduct';
import { disposalProduct } from '../disposalProduct';

it('should always call action for selling product', () => {
  const saga = disposalProduct(requestProductDisposal(randomCityProduct({}), 4));
  expect(saga.next().value).toEqual(put(disposalCompleted(randomCityProduct({}), 4)));
});

import { put } from 'redux-saga/effects';
import { purchaseCompleted, requestProductPurchase } from '../../../actions/accountBalance/purchaseProduct';
import randomCityProduct from '../../../model/__test__/randomProduct';
import { purchaseProduct } from '../purchaseProduct';

it('should always call action for buying products', () => {
  const saga = purchaseProduct(requestProductPurchase(randomCityProduct({}), 4));
  expect(saga.next().value).toEqual(put(purchaseCompleted(randomCityProduct({}), 4)));
});

import { put, takeEvery } from 'redux-saga/effects';
import { disposalCompleted, IRequestProductDisposal } from '../../actions/accountBalance/sellProduct';
import { AccountBalanceActions } from '../../reducers/accountBalance/actionNames';

function* disposalProduct(action: IRequestProductDisposal) {
  yield put(disposalCompleted(action.product, action.quantity));
}

export default [takeEvery(AccountBalanceActions.RequestProductDisposal, disposalProduct)];
export { disposalProduct };

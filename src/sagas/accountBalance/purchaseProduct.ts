import { put, takeEvery } from 'redux-saga/effects';
import { IRequestProductPurchase, purchaseCompleted } from '../../actions/accountBalance/purchaseProduct';
import { AccountBalanceActions } from '../../reducers/accountBalance/actionNames';

function* purchaseProduct(action: IRequestProductPurchase) {
  yield put(purchaseCompleted(action.product, action.quantity));
}

export default [takeEvery(AccountBalanceActions.RequestProductPurchase, purchaseProduct)];
export { purchaseProduct };

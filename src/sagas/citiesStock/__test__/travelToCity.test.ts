import { put, select, SelectEffect } from 'redux-saga/effects';
import { changeProductsInCity } from '../../../actions/citiesStock/changeProducts';
import { movePlayerToAnotherCity, requestTravelToAnotherCity } from '../../../actions/citiesStock/travelToCity';
import { ICity } from '../../../model/cities';
import findCurrentCity, { IStore } from '../select/currentCity';
import { travelToCity } from '../travelToCity';

const cities: ICity[] = [
  { name: 'A', primaryResource: 'wood', position: 1 },
  { name: 'B', primaryResource: 'water', position: 2 }
];

const fakeStore = (c: ICity[]): IStore => ({
  citiesStock: {
    travels: [{ toCity: c[0] }]
  }
});

it.skip('should call nothing when travel from city A to B is not possible', () => {
  const farAwayCities: ICity[] = [
    { name: 'A', primaryResource: 'wood', position: 1 },
    { name: 'B', primaryResource: 'water', position: 3 }
  ];

  const saga = travelToCity(farAwayCities)(requestTravelToAnotherCity('B'));
  const selectAction = saga.next().value as SelectEffect;
  selectAction.SELECT.selector(fakeStore(farAwayCities));

  expect(selectAction).toEqual(select(findCurrentCity));
  expect(saga.next(findCurrentCity(fakeStore(farAwayCities))).value).toBeUndefined();
});

it.skip('should call nothing when travel is from the same city to the same', () => {
  const farAwayCities: ICity[] = [
    { name: 'A', primaryResource: 'wood', position: 1 },
    { name: 'B', primaryResource: 'water', position: 3 }
  ];

  const saga = travelToCity(farAwayCities)(requestTravelToAnotherCity('A'));
  const selectAction = saga.next().value as SelectEffect;
  selectAction.SELECT.selector(fakeStore(farAwayCities));

  expect(selectAction).toEqual(select(findCurrentCity));
  expect(saga.next(findCurrentCity(fakeStore(farAwayCities))).value).toBeUndefined();
});

it.skip('should always call action with travelling to city', () => {
  const saga = travelToCity(cities)(requestTravelToAnotherCity('B'));
  const selectAction = saga.next().value as SelectEffect;
  selectAction.SELECT.selector(fakeStore(cities));

  expect(selectAction).toEqual(select(findCurrentCity));
  expect(saga.next(findCurrentCity(fakeStore(cities))).value).toEqual(put(movePlayerToAnotherCity('B')));
});

it.skip('should always call action to change products in stock', () => {
  const saga = travelToCity(cities)(requestTravelToAnotherCity('B'));
  const selectAction = saga.next().value as SelectEffect;
  selectAction.SELECT.selector(fakeStore(cities));

  expect(selectAction).toEqual(select(findCurrentCity));
  expect(saga.next(findCurrentCity(fakeStore(cities))).value).toEqual(put(movePlayerToAnotherCity('B')));
  expect(saga.next().value).toEqual(
    put(changeProductsInCity([{ name: 'Lorem ipsum', buyPrice: 10, sellPrice: 10, type: 'wood' }]))
  );
});

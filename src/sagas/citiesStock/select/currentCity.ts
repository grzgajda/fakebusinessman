import { ICityPosition } from '../../../model/cities';

export default (state: IStore): ICityPosition => {
  const [c] = state.citiesStock.travels.slice(-1);
  return c.toCity;
};
export interface IStore {
  citiesStock: {
    travels: ITravel[];
  };
}
interface ITravel {
  toCity: ICityPosition;
}

import { put, select, takeEvery } from 'redux-saga/effects';
import { changeProductsInCity } from '../../actions/citiesStock/changeProducts';
import { IRequestTravelToAnotherCity, movePlayerToAnotherCity } from '../../actions/citiesStock/travelToCity';
import notifyPlayer from '../../actions/userInterface/notifyPlayer';
import { ICity } from '../../model/cities';
import canTravelTo from '../../model/cities/canTravelTo';
import { forCitiesFactory } from '../../model/products';
import { CitiesStockAction } from '../../reducers/citiesStock';
import findCurrentCity from './select/currentCity';

function travelToCity(cities: ICity[]) {
  return function*(action: IRequestTravelToAnotherCity) {
    const [city] = cities.filter(c => c.name === action.cityName).slice(-1);
    const currentCity: ICity = yield select(findCurrentCity);

    if (!canTravelTo(currentCity, city) && currentCity.name !== city.name) {
      yield put(notifyPlayer(`City "${city.name}" is too far to travel.`));
    }
    if (!canTravelTo(currentCity, city) && currentCity.name === city.name) {
      yield put(notifyPlayer('You cannot travel to city where you are now.'));
    }
    if (canTravelTo(currentCity, city)) {
      yield put(movePlayerToAnotherCity(city.name));
      yield put(changeProductsInCity(forCitiesFactory(city)));
    }
  };
}

export default (cities: ICity[]) => [takeEvery(CitiesStockAction.RequestTravelToAnotherCity, travelToCity(cities))];
export { travelToCity };

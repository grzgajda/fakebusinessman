import { all } from 'redux-saga/effects';
import cities from '../model/cities';
import DisposalProductSaga from './accountBalance/disposalProduct';
import PurchaseProductSaga from './accountBalance/purchaseProduct';
import TravelToCitySaga from './citiesStock/travelToCity';

export default function*() {
  yield all([...DisposalProductSaga, ...PurchaseProductSaga, ...TravelToCitySaga(cities)]);
}

import * as React from 'react';
import { Column } from '../../components/FlexWrapper';
import NavigatorMap from '../../components/NavigatorMap';
import TitleLabel from '../../components/TitleLabel';
import SingleView from '../IndexPage/SingleView';

const CitiesView = ({ cities, selectedCity, travelToCity }: ICitiesViewProps & ICitiesViewDispatch) => (
  <SingleView x={0}>
    <Column mt={20}>
      <TitleLabel>{selectedCity.name}</TitleLabel>
    </Column>
    <Column mt={20}>
      <NavigatorMap
        resourceImage={selectedCity.primaryResource}
        selectedCity={selectedCity}
        cities={cities}
        travelToCity={travelToCity}
      />
    </Column>
  </SingleView>
);

export default CitiesView;
export interface ICitiesViewProps {
  selectedCity: ICityResource;
  cities: ICityName[];
}
export interface ICitiesViewDispatch {
  travelToCity: (city: string) => any;
}
interface ICityName {
  name: string;
  position: number;
}
interface ICityResource extends ICityName {
  primaryResource: 'blood' | 'fabric' | 'iron' | 'scale' | 'water' | 'wood';
}

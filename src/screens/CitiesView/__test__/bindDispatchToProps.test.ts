import { IRequestTravelToAnotherCity } from '../../../actions/citiesStock/travelToCity';
import bindDispatchToProps from '../store/bindDispatchToProps';

it('should call action', () => {
  const dispatch = jest
    .fn()
    .mockImplementationOnce((action: IRequestTravelToAnotherCity) => expect(action.cityName).toEqual('A'));
  bindDispatchToProps(dispatch).travelToCity('A')();
});

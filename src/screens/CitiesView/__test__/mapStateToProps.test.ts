import randomCity from '../../../model/__test__/randomCity';
import mapStateToProps, { IStateToProps } from '../store/mapPropsToState';

it('should return props for CitiesView', () => {
  const state: IStateToProps = {
    citiesStock: {
      cities: [randomCity({ name: 'A' }), randomCity({ name: 'B' })],
      travels: [{ toCity: randomCity({ name: 'B' }) }]
    }
  };
  const props = mapStateToProps(state);
  expect(props.cities.length).toEqual(2);
  expect(props.selectedCity.name).toEqual('B');
});

import { connect } from 'react-redux';
import CitiesView from './CitiesView';
import bindDispatchToProps from './store/bindDispatchToProps';
import mapStateToProps from './store/mapPropsToState';

export default connect(
  mapStateToProps,
  bindDispatchToProps
)(CitiesView);

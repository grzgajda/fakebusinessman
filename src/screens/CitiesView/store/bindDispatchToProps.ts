import { Dispatch } from 'redux';
import { IRequestTravelToAnotherCity, requestTravelToAnotherCity } from '../../../actions/citiesStock/travelToCity';
import { ICitiesViewDispatch } from '../CitiesView';

const bindDispatchToProps = (dispatch: Dispatch): ICitiesViewDispatch => ({
  travelToCity: (city: string) => (): IRequestTravelToAnotherCity => dispatch(requestTravelToAnotherCity(city))
});

export default bindDispatchToProps;

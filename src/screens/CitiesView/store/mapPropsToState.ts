import { ICity, ITravel } from '../../../model/cities';
import currentCity from '../../../utils/currentCity';
import { ICitiesViewProps } from '../CitiesView';

const mapStateToProps = (state: IStateToProps): ICitiesViewProps => ({
  cities: state.citiesStock.cities,
  selectedCity: currentCity(state.citiesStock.travels, state.citiesStock.cities)
});

export default mapStateToProps;
export interface IStateToProps {
  citiesStock: {
    cities: ICity[];
    travels: ITravel[];
  };
}

import * as React from 'react';
import SwipeableViews from 'react-swipeable-views';
import { ISwitchSelectedViewInterface } from '../../actions/userInterface/switchSelectedView';
import Notification from '../../containers/Notification';
import CitiesView from '../CitiesView';
import MarketView from '../MarketView';

const IndexPage = ({ handleChangingView, selectedView }: IIndexPageProps & IIndexPageDispatch) => (
  <React.Fragment>
    <SwipeableViews index={selectedView} onChangeIndex={handleChangingView}>
      <CitiesView />
      <MarketView />
    </SwipeableViews>
    <Notification />
  </React.Fragment>
);

export default IndexPage;
export interface IIndexPageProps {
  selectedView: number;
}
export interface IIndexPageDispatch {
  handleChangingView: (index: number) => ISwitchSelectedViewInterface;
}

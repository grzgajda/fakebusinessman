import Background from '../../components/Background';
import styled from '../../theme/styled';

const SingleView = styled(Background)`
  justify-content: flex-start;
`;

export default SingleView;

import { ISwitchSelectedViewInterface } from '../../../actions/userInterface/switchSelectedView';
import bindDispatchToProps from '../store/bindDispatchToProps';

it('should call action', () => {
  const dispatch = jest
    .fn()
    .mockImplementationOnce((action: ISwitchSelectedViewInterface) => expect(action.index).toEqual(3));
  bindDispatchToProps(dispatch).handleChangingView(3);
});

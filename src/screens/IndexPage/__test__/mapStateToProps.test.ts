import mapStateToProps, { IStateToProps } from '../store/mapStateToProps';

it('should return props for IndexPage', () => {
  const state: IStateToProps = {
    userInterface: { selectedView: 1 }
  };
  const props = mapStateToProps(state);
  expect(props.selectedView).toEqual(1);
});

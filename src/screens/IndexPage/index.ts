import { connect } from 'react-redux';
import IndexPage from './IndexPage';
import bindDispatchToProps from './store/bindDispatchToProps';
import mapStateToProps from './store/mapStateToProps';

export default connect(
  mapStateToProps,
  bindDispatchToProps
)(IndexPage);

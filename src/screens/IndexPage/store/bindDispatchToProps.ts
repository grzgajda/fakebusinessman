import { Dispatch } from 'redux';
import switchSelectedView, { ISwitchSelectedViewInterface } from '../../../actions/userInterface/switchSelectedView';
import { IIndexPageDispatch } from './../IndexPage';

const bindDispatchToProps = (dispatch: Dispatch): IIndexPageDispatch => ({
  handleChangingView: (index: number): ISwitchSelectedViewInterface => dispatch(switchSelectedView(index))
});

export default bindDispatchToProps;

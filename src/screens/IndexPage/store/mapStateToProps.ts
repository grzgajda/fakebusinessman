import { IIndexPageProps } from '../IndexPage';

const mapStateToProps = (store: IStateToProps): IIndexPageProps => ({
  selectedView: store.userInterface.selectedView
});

export default mapStateToProps;
export interface IStateToProps {
  userInterface: {
    selectedView: number;
  };
}

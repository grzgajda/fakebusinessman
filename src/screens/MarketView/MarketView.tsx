import * as React from 'react';
import BasicButton from '../../components/BasicButton';
import { Column, WrappedRow } from '../../components/FlexWrapper';
import ProductView from '../../components/ProductView';
import TitleLabel from '../../components/TitleLabel';
import { IProduct, IProductPrice } from '../../model/products';
import formatNumber from '../../utils/formatNumber';
import maxItemsToBuy from '../../utils/maxItemsToBuy';
import maxItemsToSell from '../../utils/maxItemsToSell';
import SingleView from '../IndexPage/SingleView';

const MarketView = ({
  accountBalance,
  multiplier,
  purchaseProduct,
  sellProduct,
  products,
  switchMultiplier,
  steps
}: IMarketViewProps & IMarketViewDispatch) => (
  <SingleView x={-100}>
    <Column mt={20}>
      <TitleLabel>
        {formatNumber(accountBalance)} (steps: {steps})
      </TitleLabel>
      <WrappedRow mt={20}>
        <BasicButton onClick={switchMultiplier(1)} isSelected={multiplier === 1}>
          x1
        </BasicButton>
        <BasicButton onClick={switchMultiplier(10)} isSelected={multiplier === 10}>
          x10
        </BasicButton>
        <BasicButton onClick={switchMultiplier(100)} isSelected={multiplier === 100}>
          x100
        </BasicButton>
      </WrappedRow>
      <WrappedRow mt={10}>
        {products.map((product: IHaveProduct, index: number) => (
          <ProductView
            key={`product-${index}`}
            haveNumber={product.have}
            buyRate={maxItemsToBuy(product.buyPrice, multiplier, accountBalance)}
            sellRate={maxItemsToSell(multiplier, product.have)}
            buyAction={purchaseProduct(product, maxItemsToBuy(product.buyPrice, multiplier, accountBalance))}
            sellAction={sellProduct(product, maxItemsToSell(multiplier, product.have))}
            product={product}
          />
        ))}
      </WrappedRow>
    </Column>
  </SingleView>
);

export default MarketView;
export interface IMarketViewProps {
  accountBalance: number;
  multiplier: number;
  products: IHaveProduct[];
  steps: number;
}
export interface IMarketViewDispatch {
  switchMultiplier: (value: number) => any;
  purchaseProduct: (product: IProduct & IProductPrice, quantity: number) => any;
  sellProduct: (product: IProduct & IProductPrice, quantity: number) => any;
}
export interface IHaveProduct extends IProduct, IProductPrice {
  have: number;
}

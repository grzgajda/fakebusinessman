import { IRequestProductPurchase } from '../../../actions/accountBalance/purchaseProduct';
import { IRequestProductDisposal } from '../../../actions/accountBalance/sellProduct';
import { ISwitchMultiplierInterface } from '../../../actions/userInterface/switchMultiplier';
import randomCityProduct from '../../../model/__test__/randomProduct';
import bindDispatchToProps from '../store/bindDispatchToProps';

it('should call action "purchase product"', () => {
  const dispatch = jest
    .fn()
    .mockImplementationOnce((action: IRequestProductPurchase) => expect(action.quantity).toEqual(100));

  bindDispatchToProps(dispatch).purchaseProduct(randomCityProduct({}), 100)();
});

it('should call action "sell product"', () => {
  const dispatch = jest
    .fn()
    .mockImplementationOnce((action: IRequestProductDisposal) => expect(action.quantity).toEqual(100));

  bindDispatchToProps(dispatch).sellProduct(randomCityProduct({}), 100)();
});

it('should allow to switch multiplier', () => {
  const dispatch = jest
    .fn()
    .mockImplementationOnce((action: ISwitchMultiplierInterface) => expect(action.multiplier).toEqual(10));

  bindDispatchToProps(dispatch).switchMultiplier(10);
});

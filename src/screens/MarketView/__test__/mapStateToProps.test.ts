import mapStateToProps, { IStateToProps } from '../store/mapStateToProps';

it('should return props for MarketView', () => {
  const state: IStateToProps = {
    accountBalance: {
      yourAccount: [{ amount: 100 }, { amount: 250 }]
    },
    citiesStock: {
      resources: [
        [
          { type: 'wood', buyPrice: 20, sellPrice: 30 },
          { type: 'water', buyPrice: 20, sellPrice: 30 },
          { type: 'scale', buyPrice: 20, sellPrice: 30 },
          { type: 'iron', buyPrice: 20, sellPrice: 30 },
          { type: 'fabric', buyPrice: 20, sellPrice: 30 },
          { type: 'blood', buyPrice: 20, sellPrice: 30 }
        ]
      ],
      travels: []
    },
    userInterface: {
      selectedMultiplier: 10
    }
  };
  const props = mapStateToProps(state);
  expect(props.accountBalance).toEqual(350);
  expect(props.multiplier).toEqual(10);

  const woodProduct = props.products.filter(p => p.type === 'wood');
  expect(woodProduct.length).toEqual(1);
  expect(woodProduct[0].buyPrice).toEqual(20);
  expect(woodProduct[0].sellPrice).toEqual(30);
});

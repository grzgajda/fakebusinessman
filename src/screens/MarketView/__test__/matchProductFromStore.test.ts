import matchProductFromStore, { IProduct, IProductBuySell } from '../utils/matchProductToStore';

it('should find "wood" product from latest array', () => {
  const product: IProduct = { name: 'Wood', price: 20, type: 'wood', icon: 'wood.png' };
  const products: [IProductBuySell[]] = [[{ type: 'wood', buyPrice: 30, sellPrice: 40 }]];

  const result = matchProductFromStore(product, products);
  expect(result.type).toEqual(product.type);
  expect(result.buyPrice).toEqual(30);
});

it('should find "wood" in latest array even when there is more than one array of resources', () => {
  const product: IProduct = { name: 'Wood', price: 20, type: 'wood', icon: 'wood.png' };
  const products: IProductBuySell[][] = [
    [{ type: 'wood', buyPrice: 50, sellPrice: 60 }],
    [{ type: 'wood', buyPrice: 30, sellPrice: 40 }]
  ];

  const result = matchProductFromStore(product, products);
  expect(result.type).toEqual(product.type);
  expect(result.buyPrice).toEqual(30);
});

it('should find "water" in latest array even when there is more than one product', () => {
  const product: IProduct = { name: 'Wood', price: 20, type: 'water', icon: 'wood.png' };
  const products: IProductBuySell[][] = [
    [{ type: 'water', buyPrice: 80, sellPrice: 70 }, { type: 'wood', buyPrice: 50, sellPrice: 60 }],
    [{ type: 'wood', buyPrice: 30, sellPrice: 40 }, { type: 'water', buyPrice: 100, sellPrice: 100 }]
  ];

  const result = matchProductFromStore(product, products);
  expect(result.type).toEqual(product.type);
  expect(result.buyPrice).toEqual(100);
});

it('should find "water" in any array even when there is no water in latest one', () => {
  const product: IProduct = { name: 'Wood', price: 20, type: 'water', icon: 'wood.png' };
  const products: IProductBuySell[][] = [
    [{ type: 'water', buyPrice: 80, sellPrice: 70 }, { type: 'wood', buyPrice: 50, sellPrice: 60 }],
    [{ type: 'wood', buyPrice: 30, sellPrice: 40 }, { type: 'blood', buyPrice: 100, sellPrice: 100 }]
  ];

  const result = matchProductFromStore(product, products);
  expect(result.type).toEqual(product.type);
  expect(result.buyPrice).toEqual(80);
});

it('should throw an exception when in whole resources there is no any "water" type', () => {
  const product: IProduct = { name: 'Wood', price: 20, type: 'water', icon: 'wood.png' };
  const products: IProductBuySell[][] = [
    [{ type: 'scale', buyPrice: 80, sellPrice: 70 }, { type: 'wood', buyPrice: 50, sellPrice: 60 }],
    [{ type: 'blood', buyPrice: 30, sellPrice: 40 }, { type: 'fabric', buyPrice: 100, sellPrice: 100 }]
  ];

  try {
    matchProductFromStore(product, products);
  } catch (e) {
    expect(e.message).toEqual(`Cannot find any product of type "${product.type}" in whole history of market!`);
  }
});

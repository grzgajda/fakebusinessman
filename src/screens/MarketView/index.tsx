import { connect } from 'react-redux';
import MarketView from './MarketView';
import bindDispatchToProps from './store/bindDispatchToProps';
import mapStateToProps from './store/mapStateToProps';

export default connect(
  mapStateToProps,
  bindDispatchToProps
)(MarketView);

import { Dispatch } from 'redux';
import { IRequestProductPurchase, requestProductPurchase } from '../../../actions/accountBalance/purchaseProduct';
import { IRequestProductDisposal, requestProductDisposal } from '../../../actions/accountBalance/sellProduct';
import switchMultiplier, { ISwitchMultiplierInterface } from '../../../actions/userInterface/switchMultiplier';
import { IProduct, IProductPrice } from '../../../model/products';
import { IMarketViewDispatch } from '../MarketView';

const bindDispatchToProps = (dispatch: Dispatch): IMarketViewDispatch => ({
  purchaseProduct: (product: ICityProduct, quantity: number) => (): IRequestProductPurchase =>
    dispatch(requestProductPurchase(product, quantity)),
  sellProduct: (product: ICityProduct, quantity: number) => (): IRequestProductDisposal =>
    dispatch(requestProductDisposal(product, quantity)),
  switchMultiplier: (value: number) => (): ISwitchMultiplierInterface => dispatch(switchMultiplier(value))
});

export default bindDispatchToProps;
interface ICityProduct extends IProduct, IProductPrice {}

import products, { IProduct, ProductType } from '../../../model/products';
import calculateBalance from '../../../utils/calculateBalance';
import countProducts from '../../../utils/countProducts';
import { IHaveProduct, IMarketViewProps } from '../MarketView';
import matchProductsFromStore from '../utils/matchProductToStore';

const addPriceToProducts = (state: IStateToProps) => (product: IProduct): IHaveProduct => ({
  ...product,
  buyPrice: matchProductsFromStore(product, state.citiesStock.resources).buyPrice,
  have: countProducts(state.accountBalance.yourAccount, product.name),
  sellPrice: matchProductsFromStore(product, state.citiesStock.resources).sellPrice
});

const mapStateToProps = (state: IStateToProps): IMarketViewProps => ({
  accountBalance: calculateBalance(state.accountBalance.yourAccount),
  multiplier: state.userInterface.selectedMultiplier,
  products: products.map(addPriceToProducts(state)),
  steps: state.citiesStock.travels.length
});

export default mapStateToProps;
export interface IStateToProps {
  accountBalance: {
    yourAccount: ITransaction[];
  };
  userInterface: {
    selectedMultiplier: number;
  };
  citiesStock: {
    resources: IProductBuySell[][];
    travels: [];
  };
}
interface ITransaction {
  amount: number;
}
export interface IProductBuySell {
  buyPrice: number;
  sellPrice: number;
  type: ProductType;
}

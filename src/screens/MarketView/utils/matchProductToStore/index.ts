import { IProduct } from '../../../../model/products';
import { IProductBuySell } from '../../store/mapStateToProps';

const matchProductFromStore = (product: IProduct, fromStore: IProductBuySell[][]): IProductBuySell => {
  const filterToMatchType = fromStore.filter(res => res.filter(p => p.type === product.type).length > 0);
  if (filterToMatchType.length === 0) {
    throw new Error(`Cannot find any product of type "${product.type}" in whole history of market!`);
  }

  const [last] = filterToMatchType.slice(-1);
  const my = last.filter(p => p.type === product.type);
  if (my.length === 0) {
    throw new Error(`Cannot find any product of type "${product.type}" in whole history of market!`);
  }

  return my[0];
};

export default matchProductFromStore;
export { IProduct, IProductBuySell };

import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import AccountBalanceStore, { IAccountBalanceState } from './reducers/accountBalance';
import AchievementsStore, { IAchievementsState } from './reducers/achievements';
import CitiesStockStore, { ICitiesStockState } from './reducers/citiesStock';
import UserInterfaceStore, { IUserInterfaceState } from './reducers/userInterface';
import rootSaga from './sagas';

// tslint:disable-next-line
const composeEnhancers = (window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] as typeof compose) || compose;
const sagaMiddleware = createSagaMiddleware();

export default createStore(
  combineReducers({
    accountBalance: AccountBalanceStore,
    achievements: AchievementsStore,
    citiesStock: CitiesStockStore,
    userInterface: UserInterfaceStore
  }),
  composeEnhancers(applyMiddleware(sagaMiddleware))
);

export { Provider };
export interface IStore {
  accountBalance: IAccountBalanceState;
  achievements: IAchievementsState;
  citiesStock: ICitiesStockState;
  userInterface: IUserInterfaceState;
}

sagaMiddleware.run(rootSaga);

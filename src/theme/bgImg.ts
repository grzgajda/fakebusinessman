import { css } from './styled';

const bgImg = (width: number | 'auto', height: number | 'auto', src: string) => css`
  width: ${width}px;
  height: ${height}px;
  background-repeat: no-repeat;
  background-image: url(${src});
  user-select: none;
`;

export default bgImg;

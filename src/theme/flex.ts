import { css } from './styled';

const flex = css`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default flex;

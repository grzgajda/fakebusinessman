import * as styledComponents from 'styled-components';
import arcadeFont from '../assets/fonts/arcade.ttf';
import theme, { IThemeInterface } from './theme';

const {
  default: styled,
  css,
  keyframes,
  ThemeProvider,
  createGlobalStyle
} = styledComponents as styledComponents.ThemedStyledComponentsModule<IThemeInterface>;

interface ITheme {
  theme: IThemeInterface;
}

const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: 'Arcade';
    font-style: normal;
    font-weight: normal;
    src: url(${arcadeFont});
  }
  * {
    font-family: 'Arcade', monospace;
    box-sizing: border-box;
  }
  html,
  body {
    margin: 0;
    padding: 0;
    overflow: hidden;
  }
  #root {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100vw;
    height: 100vh;
  }
`;

export default styled;
export { css, keyframes, ThemeProvider, theme, IThemeInterface, ITheme, GlobalStyles };

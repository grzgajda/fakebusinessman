export interface IThemeInterface {
  colors: {
    primary: string;
    lightPrimary: string;
  };
}

const theme: IThemeInterface = {
  colors: {
    lightPrimary: '#D6A845',
    primary: '#d27d2c'
  }
};

export default theme;

import { ITransaction } from '../../reducers/accountBalance/initialState';
import calculateBalance from './calculateBalance';

it('should take all transaction and sum balance', () => {
  const exampleData: ITransaction[] = [{ amount: 100 }, { amount: 200 }, { amount: -100 }];

  expect(calculateBalance(exampleData)).toEqual(200);
});

it('should take all transaction and sum balance even when result is negative', () => {
  const exampleData: ITransaction[] = [{ amount: 100 }, { amount: -200 }, { amount: -100 }];

  expect(calculateBalance(exampleData)).toEqual(-200);
});

it('should return 0 when list of transactions is empty', () => {
  expect(calculateBalance([])).toEqual(0);
});

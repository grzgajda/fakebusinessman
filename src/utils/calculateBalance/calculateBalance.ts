import { ITransaction } from './../../reducers/accountBalance/initialState';

const sumBalance = (transactions: ITransaction[]): number =>
  transactions.map((h: ITransaction): number => h.amount).reduce((p: number, n: number) => p + n, 0);

export default sumBalance;

import randomCityProduct from '../../model/__test__/randomProduct';
import { IPurchaseTransaction, ITransaction } from '../../reducers/accountBalance/initialState';
import countProducts from './countProducts';

it('should find all products in transactions with name "Soup" and sum them', () => {
  const exampleTransactions: ITransaction[] = [{ amount: 100 }, { amount: 200 }];
  const examplePurchases: IPurchaseTransaction[] = [
    { amount: -100, quantity: 1, product: randomCityProduct({ name: 'Soup' }) },
    { amount: -100, quantity: 1, product: randomCityProduct({ name: 'Bread' }) }
  ];

  expect(countProducts([...exampleTransactions, ...examplePurchases], 'Soup')).toEqual(1);
});

it('should not find any product with name "Soup" and returns 0', () => {
  const exampleTransactions: ITransaction[] = [{ amount: 100 }, { amount: 200 }];
  const examplePurchases: IPurchaseTransaction[] = [
    { amount: -100, quantity: 1, product: randomCityProduct({ name: 'Bread' }) }
  ];

  expect(countProducts([...exampleTransactions, ...examplePurchases], 'Soup')).toEqual(0);
});

it('should find all products named "Soup" and sum their quantities', () => {
  const exampleTransactions: ITransaction[] = [{ amount: 100 }, { amount: 200 }];
  const examplePurchases: IPurchaseTransaction[] = [
    { amount: -100, quantity: 10, product: randomCityProduct({ name: 'Soup' }) },
    { amount: -100, quantity: 15, product: randomCityProduct({ name: 'Soup' }) },
    { amount: -100, quantity: 1, product: randomCityProduct({ name: 'Bread' }) }
  ];

  expect(countProducts([...exampleTransactions, ...examplePurchases], 'Soup')).toEqual(25);
});

it('should find all products named "Soup" and find difference between purchase and sale transaction', () => {
  const exampleTransactions: ITransaction[] = [{ amount: 100 }, { amount: 200 }];
  const examplePurchases: IPurchaseTransaction[] = [
    { amount: -100, quantity: 10, product: randomCityProduct({ name: 'Soup' }) },
    { amount: -100, quantity: 15, product: randomCityProduct({ name: 'Soup' }) },
    { amount: 100, quantity: 1, product: randomCityProduct({ name: 'Bread' }) },
    { amount: 50, quantity: 5, product: randomCityProduct({ name: 'Soup' }) }
  ];

  expect(countProducts([...exampleTransactions, ...examplePurchases], 'Soup')).toEqual(20);
});

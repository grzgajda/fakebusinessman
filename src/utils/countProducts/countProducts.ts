import { IPurchaseTransaction, ITransaction } from './../../reducers/accountBalance/initialState';

const isPurchaseTransaction = (t: ITransaction): boolean => 'product' in t && 'quantity' in t;
const isMatchingName = (productName: string) => (t: IPurchaseTransaction): boolean => t.product.name === productName;
const negativeQuantity = (t: IPurchaseTransaction): number => (t.amount >= 0 ? -1 * t.quantity : t.quantity);
const sumQuantity = (p: number, c: number): number => p + c;

const countProducts = (transactions: ITransaction[], productName: string): number =>
  transactions
    .filter(isPurchaseTransaction)
    .filter(isMatchingName(productName))
    .map(negativeQuantity)
    .reduce(sumQuantity, 0);

export default countProducts;

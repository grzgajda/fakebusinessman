import randomCity from '../../model/__test__/randomCity';
import { ICity, ITravel } from '../../model/cities';
import currentCity from './currentCity';

it('should find city latest travel and match city from list of cities', () => {
  const travels: ITravel[] = [{ toCity: randomCity({ name: 'B' }) }, { toCity: randomCity({ name: 'C' }) }];
  const cities: ICity[] = [randomCity({ name: 'A' }), randomCity({ name: 'B' }), randomCity({ name: 'C' })];

  const city = currentCity(travels, cities);
  expect(city.name).toEqual('C');
});

it('should throw an error when list of travels is empty', () => {
  const cities: ICity[] = [randomCity({ name: 'A' }), randomCity({ name: 'B' }), randomCity({ name: 'C' })];

  try {
    currentCity([], cities);
  } catch (e) {
    expect(e).toBeInstanceOf(Error);
    expect(e.message).toEqual('List of travels cannot be empty');
  }
});

it('should throw an error when list of cities does not contain city', () => {
  const travels: ITravel[] = [{ toCity: randomCity({ name: 'B' }) }, { toCity: randomCity({ name: 'C' }) }];
  const cities: ICity[] = [randomCity({ name: 'B' })];

  try {
    currentCity(travels, cities);
  } catch (e) {
    expect(e).toBeInstanceOf(Error);
    expect(e.message).toEqual('Cannot find city named C');
  }
});

it('should create temporary city when list of cities is empty', () => {
  const travels: ITravel[] = [{ toCity: randomCity({ name: 'B' }) }, { toCity: randomCity({ name: 'C' }) }];

  try {
    currentCity(travels);
  } catch (e) {
    expect(e).toBeInstanceOf(Error);
    expect(e.message).toEqual('List of cities cannot be empty');
  }
});

it('should find city even when there is only one travel', () => {
  const travels: ITravel[] = [{ toCity: randomCity({ name: 'A' }) }];
  const cities: ICity[] = [randomCity({ name: 'A' }), randomCity({ name: 'B' }), randomCity({ name: 'C' })];

  const city = currentCity(travels, cities);
  expect(city.name).toEqual('A');
});

it('should not mutate travels', () => {
  const travels: ITravel[] = [{ toCity: randomCity({ name: 'B' }) }];
  const cities: ICity[] = [randomCity({ name: 'A' }), randomCity({ name: 'B' }), randomCity({ name: 'C' })];

  const city = currentCity(travels, cities);
  expect(city.name).toEqual('B');

  const cityAgain = currentCity(travels, cities);
  expect(cityAgain.name).toEqual(city.name);
});

const currentCity = <C extends ICityName>(travels: ITravel[], cities: C[] = []): C => {
  if (cities.length === 0) {
    throw new Error('List of cities cannot be empty');
  }

  const lastTravel = travels.slice(-1)[0];
  if (lastTravel === undefined) {
    throw new Error('List of travels cannot be empty');
  }

  const city = findCity(lastTravel.toCity.name, cities);
  if (city === undefined) {
    throw new Error(`Cannot find city named ${lastTravel.toCity.name}`);
  }

  return city;
};

export default currentCity;
interface ICityName {
  name: string;
}
interface ITravel {
  toCity: ICityName;
}

const findCity = <C extends ICityName>(cityName: string, cities: C[]): C | undefined =>
  cities.filter((city: ICityName) => city.name === cityName).slice(-1)[0];

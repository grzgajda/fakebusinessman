import formatNumber from './formatNumber';

it('should return "0" when number is 0', () => {
  expect(formatNumber(0)).toEqual('0');
});

it('should return original value when it is less than 1000', () => {
  expect(formatNumber(100)).toEqual('100');
});

it('should return "1k" when number is 1000', () => {
  expect(formatNumber(1000)).toEqual('1k');
});

it('should return "1.2k" when number is 1200', () => {
  expect(formatNumber(1200)).toEqual('1.2k');
});

it('should return "1.2k" when number is 1239', () => {
  expect(formatNumber(1239)).toEqual('1.2k');
});

it('should return "10k" when number is 10000', () => {
  expect(formatNumber(10000)).toEqual('10k');
});

it('should return "10k" when number is 10059', () => {
  expect(formatNumber(10059)).toEqual('10k');
});

it('should return "10.1k" when number is 10149', () => {
  expect(formatNumber(10149)).toEqual('10.1k');
});

it('should return "-1.2k" when number is -1239', () => {
  expect(formatNumber(-1239)).toEqual('-1.2k');
});

it('should round number', () => {
  expect(formatNumber(5.6000000000000005)).toEqual('6');
  expect(formatNumber(5.4000000000000005)).toEqual('5');
});

const formatNumber = (num: number): string => {
  if (num > -1000 && num < 1000) {
    return Math.round(num).toString();
  }

  const rest = num % 100;

  return ((num - rest) / 1000).toString() + 'k';
};

export default formatNumber;

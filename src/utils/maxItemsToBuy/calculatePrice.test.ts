import maxItemsToBuy from './calculatePrice';

it('should set multiplier to x1 when there is enough money on account balance', () => {
  expect(maxItemsToBuy(20, 1, 100)).toEqual(1);
});

it('should set multiplier to x10 when there is enough money on account balance', () => {
  expect(maxItemsToBuy(20, 10, 200)).toEqual(10);
});

it('should set multiplier to x20 when multiplier is set on -1', () => {
  expect(maxItemsToBuy(20, -1, 400)).toEqual(20);
});

it('should set multiplier to x20 when there is not enough money on account balance', () => {
  expect(maxItemsToBuy(20, 100, 400)).toEqual(20);
});

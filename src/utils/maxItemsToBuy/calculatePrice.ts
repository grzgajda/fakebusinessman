const maxItemsToBuy = (price: number, multiplier: number, accountBalance: number): number => {
  const maxToBuy = Math.floor(accountBalance / price);
  if (multiplier === -1) {
    return maxToBuy;
  }

  const multiplyPrice = price * multiplier;
  if (multiplyPrice > accountBalance) {
    return maxToBuy;
  }

  return multiplier;
};

export default maxItemsToBuy;

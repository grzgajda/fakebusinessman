import maxItemsToSell from './maxItemsToSell';

it('should set multiplier to x1 when there is enough products in warehouse', () => {
  expect(maxItemsToSell(1, 5)).toEqual(1);
});

it('should set multiplier to x10 when there is enough products in warehouse', () => {
  expect(maxItemsToSell(10, 15)).toEqual(10);
});

it('should set multiplier to x20 when multiplier is set on -1', () => {
  expect(maxItemsToSell(-1, 15)).toEqual(15);
});

it('should set multiplier to x10 when there is not enough products in warehouse', () => {
  expect(maxItemsToSell(20, 10)).toEqual(10);
});

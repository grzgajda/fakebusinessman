const maxItemsToSell = (multiplier: number, have: number): number => {
  if (multiplier === -1) {
    return have;
  }

  return multiplier <= have ? multiplier : have;
};

export default maxItemsToSell;

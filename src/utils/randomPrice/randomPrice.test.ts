import randomPrice, { getMinMax } from './randomPrice';

it('should return range 0 to 200 when ratio is equal to 1', () => {
  const { min, max } = getMinMax(100, 1);
  expect(min).toEqual(0);
  expect(max).toEqual(200);
});

it('should return 0 when ratio is equal to 0', () => {
  expect(randomPrice(200, 0)).toEqual(200);
});

it('should return range 10 to 190 when ratio is 0.9', () => {
  const { min, max } = getMinMax(100, 0.9);
  expect(min).toEqual(10);
  expect(max).toEqual(190);
});

it('should return range 90 to 110 when ratio is 0.1', () => {
  const { min, max } = getMinMax(100, 0.1);
  expect(min).toEqual(90);
  expect(max).toEqual(110);
});

it('should ALWAYS calculate random number when ratio is different than 0 and 1', () => {
  const firstResult = randomPrice(100, 0.5);
  const secondResult = randomPrice(100, 0.5);
  expect(firstResult).not.toEqual(secondResult);
});

it('should ALWAYS calculate random number x1000', () => {
  [...new Array(1000)].forEach(() => {
    const firstResult = randomPrice(100, 0.5);
    const secondResult = randomPrice(100, 0.5);
    expect(firstResult).not.toEqual(secondResult);
  });
});

it('should reset numbers greater than 1 to 0', () => {
  expect(randomPrice(100, 2.5)).toEqual(100);
});

it('should throw any number between 35 and 65 when for number 50 ratio is 0.3', () => {
  const { min, max } = getMinMax(50, 0.3);
  expect(min).toEqual(35);
  expect(max).toEqual(65);
});

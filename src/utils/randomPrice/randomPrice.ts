const getRandomArbitrary = ({ min, max }: IMinMax): number => Math.ceil(Math.random() * (max - min) + min);
const getMinMax = (basePrice: number, ratio: number): IMinMax => {
  const min = Math.ceil(basePrice * (1 - ratio));
  const max = Math.ceil(2 * basePrice - min);

  return { min, max };
};

const randomPrice = (basePrice: number, ratio: number): number => {
  if (ratio > 1) {
    ratio = 0;
  }

  return Math.floor(getRandomArbitrary(getMinMax(basePrice, Math.abs(ratio))));
};

export default randomPrice;
export { getMinMax, getRandomArbitrary };

interface IMinMax {
  min: number;
  max: number;
}
